package es.upm.dit.apsv.cris.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;

class TestPublicationDAO {
	private Publication p;
	private PublicationDAO pdao;

	@BeforeAll
	static void dbSetUp() throws Exception {
		CSV2DB.loadPublicationsFromCSV("publications.csv");
	}

	@BeforeEach
	  void setUp() throws Exception {
	        pdao = PublicationDAOImplementation.getInstance();
	        p = new Publication();
	        p.setId("33750378200");
	        p.setAuthors("7003699673");
	        p.setPublicationDate("2006-10-16");
	        p.setPublicationName("Optics Express");
	        p.setTitle("Perfect imaging in a homogeneous threedimensional region");
	        p.setCiteCount(0);
	}
	@Test
	void testCreate() {
		pdao.delete(p);
		pdao.create(p);
		assertEquals(p, pdao.read(p.getId()));
	}

	@Test
	void testRead() {
		assertEquals(p, pdao.read(p.getId()));
	}

	@Test
	void testUpdate() {
		String oldpdt = p.getPublicationDate();
		p.setPublicationDate("2016-10-16");
		pdao.update(p);
		assertEquals(p, pdao.read(p.getId()));
		p.setPublicationDate(oldpdt);
		pdao.update(p);
	}

	@Test
	void testDelete() {
		pdao.delete(p);
		assertNull(pdao.read(p.getId()));
		pdao.create(p);
	}
	
	@Test
	void testReadAll() {
		assertTrue(pdao.readAll().size() > 75);
	}
	
	@Test
	void testReadAllPublications() {
		assertTrue(pdao.readAllPublications("7003699673").size() > 30);
	}
}
